package inLesson;

import java.util.Scanner;

public class IntroEx {
    //Метод main (public static void main(String[] args)) является точкой входа в программу.
    //Работа нашей программы начинается именно с него
    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        int number1 = scanner.nextInt(); //scanner.nextInt() - считывает с консоли, куда мы вводим, цифры
//        int number2 = scanner.nextInt(); //scanner.nextInt() - считывает с консоли, куда мы вводим, цифры
//        int result = number1 + number2;
//        System.out.println("Результат первого сложения - " + result);
//
//        //int number; - объявление переменной
//        //int number = 10; - инициализация переменной (объявление + запись значения в переменную)
//
//        number1 = scanner.nextInt();
//        number2 = scanner.nextInt();
//        result = number1 + number2;
//        System.out.println("Результат второго сложения - " + result);
//
//        number1 = scanner.nextInt();
//        number2 = scanner.nextInt();
//        result = number1 + number2;
//        System.out.println("Результат третьего сложения - " + result);

//        System.out.println("Первое сложение");
//        sumTwoNumbers();
//        System.out.println("Второе сложение");
//        sumTwoNumbers();
//        System.out.println("Третье сложение");
//        sumTwoNumbers();
//        int finish = sumTwoNumbers(10, 15);
//        System.out.println(finish);
       int a = sumTwoNumbers(99999, "Эксперимент с short");
        System.out.println(a);
    }

    //Данный метод (подпрограмма) является процедурой. Как это понять? Есть ключевое слово - void
    //МОДИФИКАТОР_ДОСТУПА(public) STATIC|NOT_STATIC (static) ТИП_ВОЗВРАЩАЕМОГО_ЗНАЧЕНИЯ(void) НАИМЕНОВАНИЕ_МЕТОДА(ФОРМАЛЬНЫЕ ПАРАМЕТРЫ)
    public static void sumTwoNumbers() {
        Scanner scanner = new Scanner(System.in);
        int number1 = scanner.nextInt(); //scanner.nextInt() - считывает с консоли, куда мы вводим, цифры
        int number2 = scanner.nextInt(); //scanner.nextInt() - считывает с консоли, куда мы вводим, цифры
        // || - это оператор ИЛИ. Т.е. если хотя бы одно из условий истина - то следующие условия смотреть компилятор не будет
        //ему достаточно того, что одно условие истина, поэтому мы провалимся внутрь блока if
        //&& - это оператор И. Для того, что бы провалиться внутрь if, нужно что бы все условия были верны
        //TODO: таблица истинности для логических операторов
        if (number1 < 0 && number2 < 0) {
            return;
        }
        int result = number1 + number2;
        System.out.println("Результат сложения - " + result);
    }

    //Метод sumTwoNumbers является перегруженным. Т.е. есть несколько "реализаций" данного метода. Методы можно объявлять с одинаковым названием
    //и с одинаковым типом возвращаемого значения
    public static int sumTwoNumbers(int value1, int value2) {
        int result = value1 + value2;
        return result;
    }

    public static int sumTwoNumbers(int value1) {
        int result = value1 + value1;
        return result;
    }

    //Если тип возвращаемого значения не void (т.е. метод что то возвращает), то тогда
    //компилятор лезит внутрь метода, и проверяет что тип возвращаемого значения совпадал с тем, что указано в наименовании метода
    public static int sumTwoNumbers(int value1, String message) {
        System.out.println(message);
        //value1 = 100000
        //TODO: как происходит подгон переменных
        short number1 = (short) value1;
        int result = number1 + number1;
        //специальный оператор, который выходит из метода в то место, где он был вызван (если справа от return что то стоит, то он это вернет)
        return result;
    }
}
